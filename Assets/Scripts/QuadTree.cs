﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEditor;

public class QuadTree : MonoBehaviour
{
	public QuadNode RootNode = new QuadNode(0, new Rect(-4, -4, 8, 8));

	[Range(1, 6)] public int MaxLevel;

	private List<Rect> Rects = new List<Rect>();
	
	private List<QuadNode> Nodes = new List<QuadNode>();

	public const int MaxPointPerNode = 5;

	public float Cooldown;

	private float nextAvailableTime;

	private void Start()
	{
		Nodes = RootNode.GetAllSubNodes();
	}

	private void OnDrawGizmos()
	{
		foreach (var node in Nodes)
		{
			Gizmos.color = Color.black;
			Gizmos.DrawWireCube(node.Bounds.center, node.Bounds.size);
		}

		foreach (var rect in Rects)
		{
			Gizmos.color = Color.red;
			Gizmos.DrawWireCube(rect.center, rect.size);
		}
	}

	private void Update()
	{
		Vector3 mousePos = Input.mousePosition;
		mousePos.z = -Camera.main.transform.position.z;
		var MouseWorldPosition = Camera.main.ScreenToWorldPoint(mousePos);

		if (Input.GetMouseButton(0) && Time.time > nextAvailableTime)
		{
			var rect = new Rect(new Vector2(MouseWorldPosition.x, MouseWorldPosition.y), new Vector2(0.1f, 0.1f));
			Rects.Add(rect);
			RootNode.Insert(rect);
			Nodes = RootNode.GetAllSubNodes();
			nextAvailableTime = Time.time + Cooldown;
		}
	}
}