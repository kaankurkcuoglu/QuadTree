﻿using UnityEngine;
using System;
using System.Collections.Generic;

[Serializable]
public class QuadNode
{
	private const int MAX_OBJECTS = 5;
	private const int MAX_LEVELS = 5;

	public int Level;
	public List<Rect> RectsInsideNode;
	public Rect Bounds;
	public QuadNode[] ChildNodes;

	public QuadNode(int pLevel, Rect pBounds)
	{
		Level = pLevel;
		RectsInsideNode = new List<Rect>();
		Bounds = pBounds;
	}


	public void Clear()
	{
		RectsInsideNode.Clear();

		for (int i = 0; i < ChildNodes.Length; i++)
		{
			if (ChildNodes[i] != null)
			{
				ChildNodes[i].Clear();
				ChildNodes[i] = null;
			}
		}
	}


	private void Split()
	{
		ChildNodes = new QuadNode[4];
		var subWidth = Bounds.width / 2f;
		var subHeight = (Bounds.height / 2f);
		var x = Bounds.x;
		var y = Bounds.y;

		ChildNodes[0] = new QuadNode(Level + 1, new Rect(x + subWidth, y, subWidth, subHeight));
		ChildNodes[1] = new QuadNode(Level + 1, new Rect(x, y, subWidth, subHeight));
		ChildNodes[2] = new QuadNode(Level + 1, new Rect(x, y + subHeight, subWidth, subHeight));
		ChildNodes[3] = new QuadNode(Level + 1, new Rect(x + subWidth, y + subHeight, subWidth, subHeight));
	}


	/*
 * Determine which node the object belongs to. -1 means
 * object cannot completely fit within a child node and is part
 * of the parent node
 */
	private int GetIndexForRectangle(Rect rect)
	{
		int index = -1;
		double verticalMidpoint = Bounds.x + (Bounds.width / 2f);
		double horizontalMidpoint = Bounds.y + (Bounds.height / 2f);

		// Object can completely fit within the top quadrants
		bool topQuadrant = (rect.y < horizontalMidpoint && rect.y + rect.height < horizontalMidpoint);
		// Object can completely fit within the bottom quadrants
		bool bottomQuadrant = (rect.y > horizontalMidpoint);

		// Object can completely fit within the left quadrants
		if (rect.x < verticalMidpoint && rect.x + rect.width < verticalMidpoint)
		{
			if (topQuadrant)
			{
				index = 1;
			}
			else if (bottomQuadrant)
			{
				index = 2;
			}
		}
		// Object can completely fit within the right quadrants
		else if (rect.x > verticalMidpoint)
		{
			if (topQuadrant)
			{
				index = 0;
			}
			else if (bottomQuadrant)
			{
				index = 3;
			}
		}

		return index;
	}

	/*
 * Insert the object into the quadtree. If the node
 * exceeds the capacity, it will split and add all
 * objects to their corresponding nodes.
 */
	public void Insert(Rect rect)
	{
		if (HasChild())
		{
			int index = GetIndexForRectangle(rect);

			if (index != -1)
			{
				ChildNodes[index].Insert(rect);

				return;
			}
		}

		RectsInsideNode.Add(rect);

		if (RectsInsideNode.Count > MAX_OBJECTS && Level < MAX_LEVELS)
		{
			if (!HasChild())
			{
				Split();
			}

			int i = 0;
			while (i < RectsInsideNode.Count)
			{
				var node = RectsInsideNode[i];
				int index = GetIndexForRectangle(node);
				if (index != -1)
				{
					ChildNodes[index].Insert(node);
					RectsInsideNode.RemoveAt(i);
				}
				else
				{
					i++;
				}
			}
		}
	}

	public bool HasChild()
	{
		return ChildNodes != null && ChildNodes.Length > 0;
	}

	public List<QuadNode> GetAllSubNodes()
	{
		var result = new List<QuadNode>();
		result.Add(this);

		if (HasChild())
		{
			foreach (var child in ChildNodes)
			{
				result.AddRange(child.GetAllSubNodes());
			}
		}


		return result;
	}

	private bool TryGetChildNodes(QuadNode quadNode, out QuadNode[] ChildNodes)
	{
		if (quadNode.HasChild())
		{
			ChildNodes = quadNode.ChildNodes;
			return true;
		}

		ChildNodes = null;
		return false;
	}
}